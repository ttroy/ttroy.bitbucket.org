var class_vsid_common_1_1_i_pv4_tuple =
[
    [ "IPv4Tuple", "class_vsid_common_1_1_i_pv4_tuple.html#a58885efc70336d03bf194818897bfde2", null ],
    [ "IPv4Tuple", "class_vsid_common_1_1_i_pv4_tuple.html#aa2e749de3bddf1fe9fc31a334e2a11fe", null ],
    [ "dstIp", "class_vsid_common_1_1_i_pv4_tuple.html#a020b9a4b89155b0e4a796d3367e577eb", null ],
    [ "dstPort", "class_vsid_common_1_1_i_pv4_tuple.html#afc04d9dcf937c67ce60a2f2fcc0c7c10", null ],
    [ "protocol", "class_vsid_common_1_1_i_pv4_tuple.html#a6e05cf962bef53a9f3b1772a04e3878f", null ],
    [ "srcIp", "class_vsid_common_1_1_i_pv4_tuple.html#aa1f9b5071312e6062b5bd39ada560c43", null ],
    [ "srcPort", "class_vsid_common_1_1_i_pv4_tuple.html#a4092c3bd8281c8f5a8972c5c6c6f4bac", null ],
    [ "dst_ip", "class_vsid_common_1_1_i_pv4_tuple.html#a21ace381c0b90771659095178d0f15af", null ],
    [ "dst_port", "class_vsid_common_1_1_i_pv4_tuple.html#a1c1d101406314b12ce43cc1d54177736", null ],
    [ "src_ip", "class_vsid_common_1_1_i_pv4_tuple.html#a2db76e768086b4ac0aabfed850940c76", null ],
    [ "src_port", "class_vsid_common_1_1_i_pv4_tuple.html#a68b8ec74e1abb0d15ba66c48837a1662", null ],
    [ "transport", "class_vsid_common_1_1_i_pv4_tuple.html#a39c4c8ec9aaa3f80d17daaefb783beec", null ]
];