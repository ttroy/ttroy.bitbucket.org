var class_vsid_1_1_direction_changes_meter =
[
    [ "DirectionChangesMeter", "class_vsid_1_1_direction_changes_meter.html#a5bc8fb44a5318beba946f44a26f3e08b", null ],
    [ "~DirectionChangesMeter", "class_vsid_1_1_direction_changes_meter.html#aa7176f254d9c13232f77e185a088933e", null ],
    [ "calculateMeasurement", "class_vsid_1_1_direction_changes_meter.html#a1c548c4b273d9fd3b336394ff4dbf424", null ],
    [ "name", "class_vsid_1_1_direction_changes_meter.html#a0c708fed3d322241c4d33aaa1ce4f05f", null ],
    [ "ProtocolModel", "class_vsid_1_1_direction_changes_meter.html#a80219b863d4ff3456933d16bc5f73f45", null ],
    [ "ProtocolModelDb", "class_vsid_1_1_direction_changes_meter.html#a3c0d389e7a9476b06313d8fb9ca9fe68", null ]
];