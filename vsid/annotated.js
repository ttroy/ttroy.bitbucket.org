var annotated =
[
    [ "Vsid", "namespace_vsid.html", "namespace_vsid" ],
    [ "VsidCommon", "namespace_vsid_common.html", "namespace_vsid_common" ],
    [ "VsidNetfilter", "namespace_vsid_netfilter.html", "namespace_vsid_netfilter" ],
    [ "VsidPcapClassifier", "namespace_vsid_pcap_classifier.html", "namespace_vsid_pcap_classifier" ],
    [ "VsidTraining", "namespace_vsid_training.html", "namespace_vsid_training" ],
    [ "CbData", "struct_cb_data.html", "struct_cb_data" ]
];